# RPG Characters
A RPG generator project with the possibility to generate warrior, ranger, mage, and several armor and weapon types. It is further possible for the characters to gain experience and level up as well as equipping items.

# Installation
You only need JDK to run this project. 

# Design
The project is developed following the SOLID principles to maintain loose coupling between classes and to faciliate further expansion of the project, for instance by adding new types of characters and items. Additionally, some of the classes were developed using the TDD approach. 


# Demonstration
The demonstration is done in the main method. A short version is included in this chapter.

```java
// Initialize some heroes
Hero warrior = new Warrior();
Hero mage = new Mage();
Hero ranger = new Ranger();

// Observe that the heroes are initialized with correct stats
System.out.println(warrior.render());
System.out.println(mage.render());
System.out.println(ranger.render());
```
Output:
```
Warrior details: 
HP:	150
STR:	10
DEX:	3
INT:	1
LVL:	1
XP to next: 100

Mage details: 
HP:	100
STR:	2
DEX:	3
INT:	10
LVL:	1
XP to next: 100

Ranger details: 
HP:	120
STR:	5
DEX:	10
INT:	2
LVL:	1
XP to next: 100
```

Let's provide the characters with some experience and observe them levelling up.

```java
warrior.gainXp(100);
System.out.println("\nWarrior gained 100 xp:\n" + warrior.render()); // should say level 2
warrior.gainXp(110+121);
System.out.println("\nWarrior gained additional 110+121 xp:\n" + warrior.render()); // should say level 4

mage.gainXp(100+110+121+133);
System.out.println("\nMage gained 100+110+121+133 xp:\n" + mage.render());

ranger.gainXp(1000);
System.out.println("\nRanger gained 1000 xp:\n" + ranger.render());
```

Output:

```
Warrior gained 100 xp:
Warrior details: 
HP:	180
STR:	15
DEX:	5
INT:	2
LVL:	2
XP to next: 110

Warrior gained additional 110+121 xp:
Warrior details: 
HP:		240
STR:	25
DEX:	9
INT:	4
LVL:	4
XP to next: 133

Mage gained 100+110+121+133 xp:
Mage details: 
HP:	160
STR:	6
DEX:	11
INT:	30
LVL:	5
XP to next: 146

Ranger gained 1000 xp:
Ranger details: 
HP:	260
STR:	19
DEX:	45
INT:	9
LVL:	8
XP to next: 142

```

