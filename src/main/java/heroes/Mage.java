package heroes;

import items.armor.Armor;
import items.weapons.Weapon;

public class Mage extends Hero {

    //Initialize a level-1 mage hero
    public Mage(){
        super();
        this.hp = 100;
        this.str = 2;
        this.dex = 3;
        this.INT = 10;
    }

    @Override
    public void updateStats() {
        this.hp += 15;
        this.str += 1;
        this.dex += 2;
        this.INT += 5;
    }


    @Override
    public String render() {
        return "Mage details: " +
                "\nHP:\t\t" + hp +
                "\nSTR:\t" + str +
                "\nDEX:\t" + dex +
                "\nINT:\t" + INT +
                "\nLVL:\t" + getLevel() +
                "\nXP to next: " + getXpToNext();

    }


}
