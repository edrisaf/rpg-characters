package heroes;

import items.Item;
import items.Slot;
import items.armor.Armor;
import items.weapons.Weapon;
import items.weapons.WeaponType;

import java.util.HashMap;

public class Warrior extends Hero{

    //Initialize a level-1 Warrior
    public Warrior(){
        super();
        this.hp = 150;
        this.str = 10;
        this.dex = 3;
        this.INT = 1;
    }


    @Override
    public void updateStats() {
        this.hp += 30;
        this.str += 5;
        this.dex += 2;
        this.INT += 1;
    }

    @Override
    public String render() {
        return "Warrior details: " +
                "\nHP:\t\t" + hp +
                "\nSTR:\t" + str +
                "\nDEX:\t" + dex +
                "\nINT:\t" + INT +
                "\nLVL:\t" + getLevel() +
                "\nXP to next: " + getXpToNext();

    }
}
