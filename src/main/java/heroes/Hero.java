package heroes;
import items.Item;
import items.Slot;
import items.weapons.*;
import items.armor.*;

import java.util.HashMap;


public abstract class Hero {
    protected int hp;
    protected int str;
    protected int dex;
    protected int INT;
    private int level;
    private int xpToNext;
    private HashMap<Slot, Item> inventory;

    public Hero(){
        this.level = 1;
        this.xpToNext = 100;
        inventory = new HashMap<>();
    }

    //Abstract methods to be inherited by sub-classes
    public abstract void updateStats();
    public abstract String render();


    // Function to provide the character with experience, check for level and update the character's stats accordingly
    public void gainXp(int xp){
        while(xp >= xpToNext) {
            xp -= xpToNext;
            this.level++;
            this.xpToNext = (int) Math.floor(100*Math.pow(1.1,this.level-1));
            updateStats();
        }
        this.xpToNext -= xp;
    }

    //Function to wield a weapon if the character got the required level. Remove current weapon if any
    public void wieldWeapon(Item weapon){
        if (weapon.getRequiredLevel() <= this.level) { //check if the hero has the required level
            if (inventory.containsKey(weapon.getSlot())){
                inventory.remove(inventory.get(weapon.getSlot())); //remove current weapon before wielding new one
            }
            inventory.put(weapon.getSlot(), weapon);
        } else System.out.println("You do not have the required level for this item.");
    }

    //Function to equip armor if the character got the required level. If there already exists an armor piece in that
    // slot, remove it and the bonuses
    public void equipArmor(Item armor){
        if (armor.getRequiredLevel() <= this.level) {
            if (inventory.containsKey(armor.getSlot())) {
                removeArmorBonuses(armor.getSlot());
                inventory.remove(inventory.get(armor.getSlot())); //remove current armor and it's bonuses before equipping new one

            }
            inventory.put(armor.getSlot(), armor);
            addArmorBonuses(armor.getSlot());
        }else System.out.println("You do not have the required level for this item.");
    }

    //Get current armor in specific slot and deduct the bonuses
    public void removeArmorBonuses(Slot slot){
        Armor armorToRemove = (Armor)inventory.get(slot);
        this.hp -= armorToRemove.getHpBonus();
        this.str -= armorToRemove.getStrBonus();
        this.dex -= armorToRemove.getDexBonus();
        this.INT -= armorToRemove.getIntBonus();
    }

    //Get current armor in specific slot and add the bonuses
    public void addArmorBonuses(Slot slot){
        Armor armorToAdd = (Armor)inventory.get(slot);
        this.hp += armorToAdd.getHpBonus();
        this.str += armorToAdd.getStrBonus();
        this.dex += armorToAdd.getDexBonus();
        this.INT += armorToAdd.getIntBonus();
    }

    //Attack function which returns base + bonus damage based on character's attribute
    public int attack(){
        if (inventory.containsKey(Slot.WEAPON)) {
            Item currentWep = inventory.get(Slot.WEAPON);
            int baseDamage = ((Weapon) currentWep).getBaseDamage();

            switch (((Weapon) currentWep).getWeaponType()) {
                case MELEE:
                    return baseDamage + (int) Math.floor(1.5 * this.str); // base damage plus strength bonus damage
                case MAGE:
                    return baseDamage + (int) Math.floor(3 * this.INT);  // base damage plus intelligence bonus damage
                case RANGER:
                    return baseDamage + (int) Math.floor(2 * this.dex); // base damage plus dexterity bonus damage
            }
        }
        return 0;
    }

    public int getXpToNext() {
        return xpToNext;
    }

    public int getLevel() {
        return level;
    }
}
