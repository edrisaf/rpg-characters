package heroes;

import items.armor.Armor;
import items.weapons.Weapon;

public class Ranger extends Hero {

    //Initialize a level-1 ranger
    public Ranger(){
        super();
        this.hp = 120;
        this.str = 5;
        this.dex = 10;
        this.INT = 2;
    }

    @Override
    public void updateStats() {
        this.hp += 20;
        this.str += 2;
        this.dex += 5;
        this.INT += 1;
    }

    @Override
    public String render() {
        return "Ranger details: " +
                "\nHP:\t\t"  + hp +
                "\nSTR:\t" + str +
                "\nDEX:\t" + dex +
                "\nINT:\t" + INT +
                "\nLVL:\t" + getLevel() +
                "\nXP to next: " + getXpToNext();

    }
}
