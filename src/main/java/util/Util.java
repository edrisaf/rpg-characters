package util;

// NB: Ignore this class. Tried to calculate character level and xp to next level in a more efficient way by taking
// advantage of geometric series, but due to small round-off errors in java, it didnt work. For instance, when the
// character should be level 5, the method in this class returned 4.999 which was floored to 4..

public class Util {
    private final int INITIAL_XP_TO_NEXT = 100;
    private final double INCREMENT_FACTOR = 1.1;

    public int calculateNextLevel(int currentLvl, int totalXP){
        double amountLevels = Math.log(1 - ((double)totalXP/(double)100)*(1.0-INCREMENT_FACTOR))/Math.log(INCREMENT_FACTOR) + 1.0;
        int completeLevels = (int) Math.floor(amountLevels);
        return completeLevels;
    }

    public int calculateXPtoLvl(int currentLevel, int totalXP){
        double amountLevels = Math.log(1 - ((double)totalXP/(double)100)*(1.0-INCREMENT_FACTOR))/Math.log(INCREMENT_FACTOR) + 1.0;
        int completeLevels = (int) Math.floor(amountLevels);
        int xpToNext = (int) Math.floor((double)INITIAL_XP_TO_NEXT * Math.pow(INCREMENT_FACTOR, ((double)completeLevels)-1.0));
        double restXP = (amountLevels - (double)completeLevels) * xpToNext;
        xpToNext = (int) Math.floor(((double)xpToNext - restXP));
        return xpToNext;
    }
}
