import heroes.Hero;
import heroes.Mage;
import heroes.Ranger;
import heroes.Warrior;
import items.Item;
import items.Slot;
import items.armor.*;
import items.weapons.Weapon;
import items.weapons.WeaponType;

public class Main {

    public static void main(String[] args) {
        // Initialize some heroes
        Hero warrior = new Warrior();
        Hero mage = new Mage();
        Hero ranger = new Ranger();

        // Observe that the heroes are initialized with correct stats
        System.out.println(warrior.render());
        System.out.println(mage.render());
        System.out.println(ranger.render());

        // Give experience to heroes and observe the level up strategy
        warrior.gainXp(100);
        System.out.println("\nWarrior gained 100 xp:\n" + warrior.render()); // should say level 2
        warrior.gainXp(110+121);
        System.out.println("\nWarrior gained additional 110+121 xp:\n" + warrior.render()); // should say level 4

        mage.gainXp(100+110+121+133);
        System.out.println("\nMage gained 100+110+121+133 xp:\n" + mage.render());

        ranger.gainXp(1000);
        System.out.println("\nRanger gained 1000 xp:\n" + ranger.render());

        // Generate some weapons
        Item greatAxe = new Weapon("Great Axe of the Exiled", 5, WeaponType.MELEE);
        Item longBow = new Weapon("Long bow of the Lone Wolf", 10, WeaponType.RANGER);
        Item tinyWand = new Weapon("Tiny Wand of the forsaken noob", 5, WeaponType.MAGE);
        System.out.println("\n" + greatAxe.render());
        System.out.println("\n" + longBow.render());
        System.out.println("\n" + tinyWand.render());

        // Generate some armor
        Item clothLegs = new Armor("Cloth Leggings of the Magi", 10, Slot.LEGS, ArmorType.CLOTH, new Cloth());
        Item plateChest = new Armor("Plate chest of the Juggernaut", 15, Slot.BODY, ArmorType.PLATE, new Plate());
        Item leatherCoif = new Armor("Leather coif of the forsaken noob", 5, Slot.HEAD, ArmorType.LEATHER, new Leather());
        System.out.println("\n" + clothLegs.render());
        System.out.println("\n" + plateChest.render());
        System.out.println("\n" + leatherCoif.render() + "\n");

        // Demonstrating hero attack function with no weapon equipped, as well as equipping item with higher req lvl
        warrior.wieldWeapon(greatAxe); // He will fail to equip due to required level
        System.out.println("Warrior without weapon dealt damage of " + warrior.attack());

        // Lets give the heroes some xp and try again
        warrior.gainXp(2000);
        mage.gainXp(2000);
        ranger.gainXp(2000);
        System.out.println("\nWarrior gained additional 2000 xp\n" + warrior.render());
        warrior.wieldWeapon(greatAxe);
        System.out.println("\nWarrior wields " + greatAxe.getName() + " which has a base damage of " + ((Weapon)greatAxe).getBaseDamage());
        System.out.println("Warrior attacked and dealt damage of " + warrior.attack());

        // Change warrior's weapon
        warrior.wieldWeapon(tinyWand);
        System.out.println("\nWarrior wields " + tinyWand.getName() + " which has a base damage of " + ((Weapon)tinyWand).getBaseDamage());
        System.out.println("Warrior attacked and dealt damage of " + warrior.attack());

        // Equip a hero with armor
        System.out.println("\nBefore wearing cloth armor\n" + ranger.render());
        System.out.println("\nCloth leggings\n" + clothLegs.render());
        ranger.equipArmor(clothLegs);
        System.out.println("\nRanger stats after equipping cloth leggings\n" + ranger.render());

        // Change the ranger's current leggings with a new fresh pants
        Item freshPants = new Armor("Fresh pants of Bel Air", 5, Slot.LEGS, ArmorType.LEATHER, new Leather());
        System.out.println("\n" + freshPants.render());
        ranger.equipArmor(freshPants);
        System.out.println("\nRanger stats after wearing fresh pants \n" + ranger.render());
    }
}
