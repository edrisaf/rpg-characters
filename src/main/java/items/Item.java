package items;

public abstract class Item {

    protected int requiredLevel;
    protected String name;
    protected Slot slot;

    public Item(String name, int requiredLevel, Slot slot){
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    public abstract String render();

    public int getRequiredLevel(){
        return requiredLevel;
    }

    public Slot getSlot(){
        return slot;
    }

    public String getName(){
        return this.name;
    }
}
