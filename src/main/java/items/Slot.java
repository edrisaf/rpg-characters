package items;

public enum Slot {
    WEAPON,
    HEAD,
    LEGS,
    BODY
}
