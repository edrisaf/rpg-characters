package items.weapons;

import items.Item;
import items.Slot;

public class Weapon extends Item {
    WeaponType weaponType;

    public Weapon(String name, int requiredLevel, WeaponType weaponType){
        super(name, requiredLevel, Slot.WEAPON);
        this.weaponType = weaponType;
    }

    //Derive base damage for weapons
    public int getBaseDamage(){
        int damage = 0;
        switch (weaponType){
            case MELEE -> damage = 15 + 2*requiredLevel;
            case RANGER -> damage = 5 + 3*requiredLevel;
            case MAGE -> damage = 25 + 2*requiredLevel;
        }
        return damage;
    }


    public WeaponType getWeaponType(){
        return this.weaponType;
    }

    @Override
    public String render() {
        return "Item stats for: " + this.name +
                "\nWeapon Type: " + this.weaponType +
                "\nWeapon level: " + this.requiredLevel +
                "\nDamage: " + getBaseDamage();
    }
}
