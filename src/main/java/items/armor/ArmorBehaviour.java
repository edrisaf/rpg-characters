package items.armor;

import items.Slot;

import java.util.HashMap;

public interface ArmorBehaviour {

    void getBonusStats(HashMap<String, Integer> stats, int armorLevel, Slot slot);

    String render(HashMap<String, Integer> stats);

}
