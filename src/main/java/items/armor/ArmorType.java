package items.armor;

public enum ArmorType {
    CLOTH,
    LEATHER,
    PLATE
}
