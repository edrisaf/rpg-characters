package items.armor;

import items.Slot;

import java.util.HashMap;

public class Plate implements ArmorBehaviour {
    double armorScaler;

    //Function to derive stats for armor type of Plate and populate hashmap
    @Override
    public void getBonusStats(HashMap<String, Integer> stats, int armorLevel, Slot slot) {
        switch(slot){
            case BODY -> armorScaler = 1.0;
            case HEAD -> armorScaler = 0.8;
            case LEGS -> armorScaler = 0.6;
        }
        int hp = (int) Math.floor((30 + 12*armorLevel)*armorScaler);
        int str = (int) Math.floor((3 + 2*armorLevel)*armorScaler);
        int dex = (int) Math.floor((1 + armorLevel)*armorScaler);

        stats.put("hp", hp);
        stats.put("str", str);
        stats.put("dex", dex);
    }

    @Override
    public String render(HashMap<String, Integer> stats) {
        return "\nBonus HP: " + stats.get("hp") +
                "\nBonus Str: " + stats.get("str") +
                "\nBonus Dex: " + stats.get("dex");
    }
}
