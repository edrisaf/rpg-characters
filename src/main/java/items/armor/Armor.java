package items.armor;

import items.Item;
import items.Slot;

import java.util.HashMap;

public class Armor extends Item {

    ArmorType type;
    ArmorBehaviour strategy;
    HashMap<String, Integer> stats;

    public Armor(String name, int requiredLevel, Slot slot, ArmorType type, ArmorBehaviour strategy){
        super(name, requiredLevel, slot);
        this.strategy = strategy;
        this.type = type;
        this.stats = new HashMap<String, Integer>();
        strategy.getBonusStats(stats, getRequiredLevel(), getSlot()); //populate the hashmap with stats
    }

    //Get base health bonus
    public int getHpBonus(){
        return stats.get("hp");
    }

    //Get base strength bonus if the armor piece provides any
    public int getStrBonus(){
        if (stats.containsKey("str")) return stats.get("str");
        return 0;
    }

    //Get base intelligence bonus if the armor piece provides any
    public int getIntBonus(){
        if (stats.containsKey("INT")) return stats.get("INT");
        return 0;
    }

    //Get dexterity bonus if the armor piece provides any
    public int getDexBonus(){
        if (stats.containsKey("dex")) return stats.get("dex");
        return 0;
    }

    public String render(){
        return "Item stats for: " + this.name +
                "\nArmor type: " + this.type +
                "\nSlot: " + this.slot +
                "\nArmor level: " + this.requiredLevel +
                strategy.render(stats);
    }
}
