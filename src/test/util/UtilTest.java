package util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UtilTest {
    Util util = new Util();
    @Test
    void calculateNextLevel() {
        assertEquals(2, util.calculateNextLevel(1, 100));
        assertEquals(8, util.calculateNextLevel(1, 1000));
    }

    @Test
    void calculateXPtoLvl() {
        assertEquals(110, util.calculateXPtoLvl(1,100));
        // does not pass due to small round-off errors in java
        assertEquals(146, util.calculateXPtoLvl(1,100+110+121+133));
    }
}