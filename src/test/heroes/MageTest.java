package heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    Mage mage = new Mage();

    @Test
    void testGainXp(){
        mage.gainXp(100);
        assertEquals(2, mage.getLevel());
        assertEquals(110, mage.getXpToNext());

        mage.gainXp(110+121);
        assertEquals(4, mage.getLevel());
        assertEquals(133, mage.getXpToNext());

        mage.gainXp(133+146);
        assertEquals(6, mage.getLevel());
        assertEquals(161, mage.getXpToNext());

    }

    @Test
    void testUpdateStats(){
        mage.gainXp(210);
        assertEquals(100+15+15, mage.hp);
        mage.gainXp(121);
        assertEquals(100+15+15+15, mage.hp);
    }
}