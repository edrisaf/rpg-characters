package items.armor;

import items.Item;
import items.Slot;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {
    Item clothLegs = new Armor("Cloth Leggings of the Magi", 10, Slot.LEGS, ArmorType.CLOTH, new Cloth());
    Item plateChestJuggernaut = new Armor("Plate Chest of the Juggernaut", 15, Slot.BODY, ArmorType.PLATE, new Plate());
    Item leatherCoif = new Armor("Leather Coif of the forsaken noob", 5, Slot.HEAD, ArmorType.LEATHER, new Leather());

    @Test
    void getHpBonus() {
        assertEquals(36, ((Armor)clothLegs).getHpBonus()); // (10+5*10)*0.6 = 36
        assertEquals(210, ((Armor)plateChestJuggernaut).getHpBonus()); // (30+12*15) = 210
        assertEquals(48, ((Armor)leatherCoif).getHpBonus());  // (20+8*5)*0.8 = 60*0.8 = 48
    }

    @Test
    void getStrBonus() {
        assertEquals(33, ((Armor)plateChestJuggernaut).getStrBonus()); // (3+2*15) = 33
        assertEquals(4, ((Armor)leatherCoif).getStrBonus());  // (1+5)*0.8 = 4.8 -> 4
    }

    @Test
    void getIntBonus() {
        assertEquals(13, ((Armor)clothLegs).getIntBonus()); // (3+2*10)*0.6 = 13.8 -> 13
    }

    @Test
    void getDexBonus() {
        assertEquals(6, ((Armor)clothLegs).getDexBonus()); // (1+10)*0.6 = 6.6 -> 6
        assertEquals(16, ((Armor)plateChestJuggernaut).getDexBonus()); // 1+15
        assertEquals(10, ((Armor)leatherCoif).getDexBonus());  // (3+2*5)*0.8 = 10.4 -> 10
    }
}